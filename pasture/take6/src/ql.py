# -*- coding: utf-8 -*-

import tensorflow as tf
import sine


MAX_NUM_OF_EP = 1000
MAX_TIME_STEP = 800
NUM_OF_STATE = 10
NUM_OF_ACTION = 3

# Borrowed from MNIST tutorial, as usual :)
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def get_state(t):
    """ return [f(t - w), ..., f(t - 1)]
    """
    return map(sine.f, range(t - 10, t))


def Q_learning():
    """ Q-learning algorithm function

    Args:
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:

    """

    # discount rate
    gamma = 0.9

    # OK. If you insist, I'll provide a bunch of batches this time
    # And, more importantly, that implies that we'll naturally
    # implement Q and \hat{Q}, and (possibly) experience replay.

    # S (None)x10
    S = tf.placeholder(tf.float32, [None, NUM_OF_STATE])

    # theta 10x3
    theta = weight_variable([NUM_OF_STATE, NUM_OF_ACTION])
    theta_t = tf.Variable(theta_b.initialized_value())

    # b 3 ???? b's shape [3]?
    # yep, TensorFlow will augment dimension.
    # You can even add a real number to any Tensor.
    b = bias_variable([NUM_OF_ACTION])
    b_t = tf.Variable(b_b.initialized_value())

    # Be careful about Q (action-value function)
    # and \hat{Q} (target action-value function)

    Q = -tf.nn.relu(tf.matmul(S, theta_b) + b_b)
    Q_t = -tf.nn.relu(tf.matmul(S, theta_t) + b_t)

    sess = tf.Session()
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        print('episode #= ' + str(ep))

        #   \STATE Initilize state $s_1$
        # Nope. We'll make a pile of states instead.

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):
            pass

            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state

            # HERE!!!!!

            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$

            # Every C steps reset \hat{Q} = Q
            # What about C = T !!!

        #   \ENDFOR

    # replay at each end of the episode.

    #   \ENDFOR

def main():
    Q_learning()


if __name__ == "__main__":
    main()
