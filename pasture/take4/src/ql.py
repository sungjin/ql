# -*- coding: utf-8 -*-

# Disclaimer: I hereby intentionally break the coding convention,
# i.e., never to use capital letter for functions.
# In this code, uppercase letter will be used for functions (!),
# and lowercase letter for variables.

import tensorflow as tf
import random
import math
import sine


# Borrowed from MNIST Experts tutorial. :(
def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


MAX_NUM_OF_EP = 1000
MAX_TIME_STEP = 800
DIM_OF_STATE = 10

actions = [{'id': 0, 'action': -1},
           {'id': 1, 'action': 0},
           {'id': 2, 'action': 1}]


def get_state(t):
    return sine.trace(t)


def initialize_Q(S, A):
    """ Initialize action-value function Q
    Args:
    S: state placeholder
    A: action placeholder

    Returns:
    Q: action-value function Q initialized with random weights \theta
    """

    # theta = tf.Variable(tf.zeros([(S.get_shape()[0] + 1), 1]))
    # b = tf.Variable(tf.zeros([1, 1]))

    theta = weight_variable([int(S.get_shape()[0] + 1), 1])
    b = bias_variable([1, 1])

    m = tf.pack([tf.concat(0, [S, tf.pack([A])])])
    r = -tf.nn.relu(tf.matmul(m, theta) + b)

    return tf.unpack(tf.unpack(r, 1)[0], 1)[0]


def select_action(Q, S, A, s, a_s, sess):
    """ Select action a according to \varepsilon greedy method.

    Args:
    s: state

    Returns:
    a: action selected by \varepsilon greedy method.

    """

    epsilon = 0.1

    p = random.uniform(0, 1)

    if (p < epsilon):
        # select random action.
        a = random.choice(a_s)
    else:
        q_s = []

        for a in a_s:
            q_s.append(sess.run(Q, feed_dict={S: s, A: a}))

        # what about tie-break?
        a = a_s[q_s.index(max(q_s))]

    return a


def execute_action(a, t, actions):
    """
    Args:
    a: action
    t: time
    actions: set of action tuples

    Returns:
    r: immediate reward
    s_prime: next state

    """

    # there must be a function doing this!
    def lookup(a):
        for action in actions:
            if action['id'] == a:
                return action['action']

    delta = lookup(a)

    r = - abs(math.pow((sine.f(t + 1) - (sine.f(t) + delta)), 2))
    s_prime = get_state(t + 1)

    return r, s_prime


# Aux function is ugly by nature!
def replay(Q, S, A, a_s, actions, sess, ep):
    f = open("./result/r%010d" % ep, 'w')

    v = 60.0
    s = get_state(1)

    def lookup(a):
        for action in actions:
            if action['id'] == a:
                return action['action']

    for t in range(1, MAX_TIME_STEP):

        q_s = []
        for a in a_s:
            q_s.append(sess.run(Q, feed_dict={S: s, A: a}))

        a = a_s[q_s.index(max(q_s))]

        r, s_prime = execute_action(a, t, actions)

        delta = lookup(a)

        v += delta

        s = s_prime

        f.write("%d\t%d\n" % (t, v))


def Q_learning(actions):
    """ Q-learning algorithm function

    Args:
    actions: set of tuples (id, action)

    Returns:

    """

    # discount rate
    gamma = 0.9

    a_s = [a['id'] for a in actions]

    # step 0: First setup the variables and placeholders.
    with tf.device('/cpu:0'):
        S = tf.placeholder(tf.float32, [DIM_OF_STATE])
        A = tf.placeholder(tf.float32, [])
        Y = tf.placeholder(tf.float32, [])

        Q = initialize_Q(S, A)
        L = tf.pow(tf.sub(Y, Q), 2)

        Op = tf.train.GradientDescentOptimizer(0.5).minimize(L),

    sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        print('episode #= ' + str(ep))

        #   \STATE Initilize state $s_1$
        s = get_state(1)

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):

            # print('t = ' + str(t))

            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}
            a = select_action(Q, S, A, s, a_s, sess)

            # print('selected action id: ' + str(a))

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state
            #   $s_{t+1}$
            r, s_prime = execute_action(a, t, actions)
            # print('r = ' + str(r))

            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$
            if (t + 1 >= MAX_TIME_STEP):
                y = r
            else:
                q_s = []
                for a_prime in a_s:
                    q_s.append(sess.run(Q,
                                        feed_dict={S: s_prime,
                                                   A: a_prime}))

                max_q = max(q_s)

                y = r + gamma * max_q

            # print('y = ' + str(y))

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$

            sess.run(Op, feed_dict={S: s, A: a, Y: y})

            s = s_prime
        #   \ENDFOR

        # replay at each end of the episode.
        replay(Q, S, A, a_s, actions, sess, ep)
    #   \ENDFOR


def main():
    Q_learning(actions)


if __name__ == "__main__":
    main()
