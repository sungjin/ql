# some elementary set operations.

s = set()
s.add(-1)
s.add(0)
s.add(1)
# or
s = {-1, 0, 1}
sorted(s)

# type(sorted(s)) = list!!!
type(sorted(s))


# tuple. We need tuple for <s,a> -> R (maybe not :).
t = (1, 2)

# that is, we need Q(s,a) = R.  Given s and a, what is the value of Q?

s = (1, 2)
v = 1
t = (s, v)



Q = [None] * 5

for i in range(5):
    Q[i] = [None] * 2
    for j in range(2):
        Q[i][j] = [0] * 3


[[[0, 0, 0], [0, 0, 0]],
 [[0, 0, 0], [0, 0, 0]],
 [[0, 0, 0], [0, 0, 0]],
 [[0, 0, 0], [0, 0, 0]],
 [[0, 0, 0], [0, 0, 0]]]

