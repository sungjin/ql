#!/bin/sh

for i in $(ls r*); do
    gnuplot <<EOF
set terminal jpeg
set output "$i.jpeg"
set grid
plot[0:800][0:120] "$i"
EOF
done
