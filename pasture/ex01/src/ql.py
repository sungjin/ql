# q.py

from math import sin
from math import pi

import random

# auxiliary functions
def init_q():	# WHAT?
    # Q(t, y, a)
    # 800 = # of t, 120 = max value of y, 3 = # of available actions
    NUM_T = 801		# 800 + 1 for S' !!!
    MAX_VAL = 120
    NUM_ACT = 3
    q = [None] * NUM_T

    for i in range(NUM_T):
        q[i] = [None] * MAX_VAL
        for j in range(MAX_VAL):
            q[i][j] = [0] * NUM_ACT

    return q

def get_q(q, s, a):
    return q[s[0]][s[1]][a + 1]	# a + 1 ??????

def set_q(q, s, a, v):
    q[s[0]][s[1]][a + 1] = v	# a + 1 ??????

# choose and return an action A from S and Q
def choose(q, s):
    epsilon = 0.1

    action_list = [-1, 0, 1]

    if (s[1] == 0):
        action_list.remove(-1)
    elif (s[1] == 119):
        action_list.remove(1)

    value_list = [None] * len(action_list)

    for i, a in enumerate(action_list):
        value_list[i] = get_q(q, s, a)

    p = random.uniform(0, 1)

    if (p >= epsilon):
        a = action_list[value_list.index(max(value_list))]
    else:
        a = action_list[random.randrange(0,len(action_list))]

    return a

def max_q(q, s):
    action_list = [-1, 0, 1]

    if (s[1] == 0):
        action_list.remove(-1)
    elif (s[1] == 119):
        action_list.remove(1)

    value_list = [None] * len(action_list)

    for i, a in enumerate(action_list):
        value_list[i] = get_q(q, s, a)

    return max(value_list)

def take_action(s, a):
    s_prime = (s[0] + 1, s[1] + a)

    t = s_prime[0]
    y = s_prime[1]

    f = 50 * sin((2 * pi * t) / 720) + 50
    g = 10 * sin((2 * pi * t) / 720) + 10
#    h = f + g
    h = f	# NOT WORK????????????????????????????????????????????
    h = f + 10	# AND THIS WORKS ?????????????????????????????????????
    h = h - 10	# AND THIS NOT WORK???????????????????????????????????
    h = f + 10	# AND THIS WORKS ?????????????????????????????????????
    r = - abs((h - y)  * (h - y))	# Reward should be negative!

    return r, s_prime

def replay(q, ep):
    f = open("./result/r%010d" % ep, 'w')

    v = 60
    action_list = [-1, 0, 1]

    for t in range(800):
        f.write("%d\t%d\n" % (t, v))
        qa = q[t][v]
        a = action_list[qa.index(max(qa))]
        v = v + a
        if (v <= 0):
            v = 0
        if (v >= 119):
            v = 119


def q_learning():

    alpha = 0.1		# learning rate
    gamma = 0.9		# discount factor
    # Initialize Q(s,a) \forall s, a, arbitrarily,
    # Q(terminal-state, ) = 0

    q = init_q()

    ep_num = 1
    # Repeat (for each episode):
    while True:
        print("episode: %d" % ep_num)
        # Initialize S. S(t, y). 60 = 50 sin(0) + 10 sin(0)
        s = (0, 60)
        # Repeat (for each step of episode):
        for i in range(800):
            # Choose A from S using policy derived from Q ( \epsilon
            # greedy)
            a = choose(q, s)
            # Take action A, observe R, S'
            r, s_prime = take_action(s, a)
            # Update Q(S,A)
            q_old = get_q(q, s, a)
            q_new = q_old + alpha * (r + gamma * max_q(q, s_prime) -
                                     q_old)
            set_q(q, s, a, q_new)
            # Update S (S <- S')
            s = s_prime
            # Until S is terminal

        replay(q, ep_num)
        ep_num = ep_num + 1

