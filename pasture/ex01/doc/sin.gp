set grid
f(x) =  50 * sin((2 * pi * x) / 720) + 50
g(x) =  10 * sin((2 * pi * x) / 360) + 10
plot [0:800][-10:130] f(x) + g(x)