# -*- coding: utf-8 -*-

import math


def f(t):
    return 50.0 * math.sin(t * 2.0 * math.pi / 720.0) + 60.0


def trace(t):
    return [f(t_) for t_ in range(t - 10, t)]
