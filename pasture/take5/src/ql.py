# -*- coding: utf-8 -*-

# In this trial we will use the Q function as the form of Q(S)(A),
# that is, Q(S)(A) = QS(A) will be implented as a NN.

import tensorflow as tf
import random
import math

import sine


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

MAX_NUM_OF_EP = 1000
MAX_TIME_STEP = 800
NUM_OF_STATE = 10
NUM_OF_ACTION = 3

actions = [-1, 0, 1]


def get_state(t):
    return [sine.trace(t)]


def select_action(Q, S, s, sess):
    """ Select action a according to \varepsilon greedy method.

    Args:

    Returns:
    a: action selected by \varepsilon greedy method.

    """
    # LET'S WEAVE THE CODE FROM HERE.

    epsilon = 0.1
    p = random.uniform(0, 1)

    if (p < epsilon):
        # select random action.
        # Wait! A must be a tensor, or not?
        a = random.randrange(0, NUM_OF_ACTION)
    else:
        a_a = sess.run(tf.argmax(Q, 1), feed_dict={S: s})
        # I'm afraid of this way of obtaining value from TensorFlow
        # Yes, I was right. This is more expensive than it looks.

        # Houston, we have a problem.

        # Feeding an element and fetching a single number is last
        # thing to play with TensorFlow.

        # Then, how can we overcome this?

        a = a_a[0]

    return a


def execute_action(a, t, actions):
    """
    Args:
    a: action id
    t: time
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:
    r: immediate reward
    s_prime: next state

    """

    delta = actions[a]

    r = - abs(math.pow((sine.f(t + 1) - (sine.f(t) + delta)), 2))
    s_prime = get_state(t + 1)

    return r, s_prime


def Q_learning(actions):
    """ Q-learning algorithm function

    Args:
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:

    """
    # discount rate
    gamma = 0.9

    # S 1x10
    S = tf.placeholder(tf.float32, [1, NUM_OF_STATE])

    # theta 10x3
    theta = weight_variable([NUM_OF_STATE, NUM_OF_ACTION])
    # b 1x3
    b = bias_variable([1, NUM_OF_ACTION])

    Q = -tf.nn.relu(tf.matmul(S, theta) + b)

    sess = tf.Session()
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        print('episode #= ' + str(ep))

        #   \STATE Initilize state $s_1$
        s = get_state(1)

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):

            print('t = ' + str(t))

            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}

            # LET'S WEAVE THE CODE FROM HERE.

            a = select_action(Q, S, s, sess)

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state
            r, s_prime = execute_action(a, t, actions)

            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$

            if (t + 1 >= MAX_TIME_STEP):
                y = r
            else:
                q_a = sess.run(tf.reduce_max(Q, 1), feed_dict={S: s_prime})
                max_q = q_a[0]

                y = r + gamma * max_q

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$

            # ?????????????????????????????????????????????????

            s = s_prime
    # ---------------------------------------------------------
    # ---------------------------------------------------------
    # ---------------------------------------------------------

def main():
    Q_learning(actions)


if __name__ == "__main__":
    main()
