# Auxiliary module sine.
import numpy as np

def f(t):
    return 50.0 * np.sin( 2.0 * np.pi * t / 720.0) + 60.0

def trace(t):
    return [f(t_) for t_ in range(t - 10, t)]
