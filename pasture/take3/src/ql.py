# Q-learning, NN version.

# Disclaimer: I hereby intentionally break the coding convention,
# i.e., never to use capital letter for functions.
# In this code, uppercase letter will be used for functions (!),
# and lowercase letter for variables.
# Afterall, what we deal with is the Q function.

import tensorflow as tf
import numpy as np
import random
import math

import sine

# This time let's make some basic structures.
# These values also should be more structured in a concise manner.
MAX_NUM_OF_EP = 1000
MAX_TIME_STEP = 800
DIM_OF_STATE = 10

actions = [{'id': 0, 'action': -1},
           {'id': 1, 'action': 0},
           {'id': 2, 'action': 1}]


def initialize_Q(S, A):
    """ Initialize action-value function Q
    Args:
    S: state placeholder
    A: action placeholder

    Returns:
    Q: action-value function Q initialized with random weights \theta
    """

    theta = tf.Variable(tf.zeros([(S.get_shape()[0] + 1), 1]))
    b = tf.Variable(tf.zeros([1, 1]))

    # m = tf.concat(1, [s, a])
    # q = -tf.nn.relu(tf.matmul(m, theta) + b)

    # def Q(S, A):
    #     m = tf.concat(1, [S, A])
    #     return -tf.nn.relu(tf.matmul(m, theta) + b)

    # for test.
    # def Q(S, A):
    #     return -tf.nn.relu(A)

    # # Q has to be a Tensor. I guess.
    # m = tf.concat(1, [S, A])
    # Q = -tf.nn.relu(tf.matmul(m, theta) + b)

    # What the...
    def Q(S, A):
        m = tf.pack([tf.concat(0, [S, tf.pack([A])])])
        r = -tf.nn.relu(tf.matmul(m, theta) + b)
        return tf.unpack(tf.unpack(r, 1)[0], 1)[0]
        # return -tf.nn.relu(tf.matmul(m, theta) + b)
        # Watch the relu in final output!

    return Q


def get_s(t):
    return sine.trace(t)

def initialize_s():
    """ Initialize state s appropriately.
    Args:

    Returns:
    s1: initial state s_1
    """

    s1 = get_s(1)

    return s1



def select_action(Q, S, A, s, a_s, sess):
    """ Select action a according to \varepsilon greedy method.

    Args:
    s: state

    Returns:
    a: action selected by \varepsilon greedy method.

    """

    epsilon = 0.1

    p = random.uniform(0, 1)

    if (p < epsilon):
        # select random action.
        a = random.choice(a_s)
    else:
        # select an action according to the greedy policy.
        idx = sess.run(tf.arg_max(tf.map_fn(lambda a: Q(S, a),
                                            tf.to_float(a_s)), 0),
                       feed_dict = {S: s})
        a = a_s[idx]

    return a


def execute_action(a, t, actions):
    """
    Args:
    a: action
    t: time

    Returns:
    r: immediate reward
    s_prime: next state

    """

    # there must be a function doing this!
    def lookup(a):
        for action in actions:
            if action['id'] == a:
                return action['action']

    delta = lookup(a)

    # reward = -abs([f(t + 1) - {f(t) + action_value}]^2)
    r = - abs(math.pow((sine.f(t + 1) - (sine.f(t) + delta)), 2))
    s_prime = get_s(t + 1)

    return r, s_prime


# Aux function is ugly by nature!
def replay(Q, S, A, a_s, actions):
    f = open("./result/r%010d" % ep, 'w')

    v = 60.0
    s = initialize_s()

    def lookup(a):
        for action in actions:
            if action['id'] == a:
                return action['action']

    for t in range(1, MAX_TIME_STEP):
        # ???
        idx = sess.run(tf.arg_max(tf.map_fn(lambda a: Q(S, a),
                                            tf.to_float(a_s)), 0),
                       feed_dict = {S: s})
        a = a_s[idx]

        r, s_prime = execute_action(a, t, actions)

        delta = lookup(a)

        v += delta

        s = s_prime

        f.write("%d\t%d\n" % (t, v))




def Q_learning(actions):
    """ Generic(! to be implemented :) q-learning algorithm function

    Args:
    actions: set of tuples (id, action)

    Returns:

    """

    # numpy array????????? I don't like it!
    # Let's fix this ugly expression!
    #a_s = np.array([a['id'] for a in actions], dtype=np.float32)
    a_s = [a['id'] for a in actions]

    # step 0: First setup the variables and placeholders.
    # Be careful of the type! A takes a real value, while ids are
    # integers.
    # By the way, in the next version, we will use softmax (maybe).
    S = tf.placeholder(tf.float32, [DIM_OF_STATE])
    A = tf.placeholder(tf.float32, [])

    #   \STATE Initialize action-value function $Q$ with random
    #   weights $\theta$

    # q is not a Tensor, but a wrapper function!
    # Who cares?
    # q is a Tensor, again.
    # q is a function, again.
    Q = initialize_Q(S, A)

    sess = tf.Session()
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        # print('episode #= ' + str(ep))

        #   \STATE Initilize state $s_1$
        s1 = initialize_s()
        s = s1

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):
            # it's worth to do???
            print('t = ' + str(t))
            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}

            a = select_action(Q, S, A, s, a_s, sess)

            # print('selected action id: ' + str(a))

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state
            #   $s_{t+1}$

            r, s_prime = execute_action(a, t, actions)
            # print('r = ' + str(r))

            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$
            if (t + 1 >= MAX_TIME_STEP):
                y = r
            else:
                max_Q = sess.run(tf.reduce_max(tf.map_fn(lambda a: Q(S,a),
                                                         tf.to_float(a_s)),
                                                         0),
                                 feed_dict = {S: s_prime})
                y = r + max_Q

            # print('y = ' + str(y))

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$

            # HOUSTON, WE HAVE A PROBLEM.

            # We had to make Q AS Tensor, NOT a functional!
            # This cost is way too expensive, making gpu like 8086!
            # Forget about this code. Rewrite Q as tensor.

        #   \ENDFOR

    # replay at each end of the episode.
    replay(Q, S, A, a_s, actions)
    #   \ENDFOR


def main():
    Q_learning(actions)


if __name__ == "__main__":
    main()
