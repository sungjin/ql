import tensorflow as tf
sess = tf.InteractiveSession()
s = tf.placeholder(tf.float32)
a = tf.placeholder(tf.float32)
s = tf.Variable(0.0)
a = tf.Variable(1.0)
sess.run(tf.initialize_all_variables())
a.eval()
s.eval()
a.assign(10.0)
a.eval()
sess.run(a.assign(10.0))
a.eval()
tf.add(s, a).eval()
def add1(a):
    return tf.add(1.0, a)
add1(a).eval()
add1(2.5).eval()
s.eval()
s.assign(111.0)
sess.run(s.assign(111.0))
def adds(a):
    return tf.add(s, a)
adds(111.0).eval()
tf.map_fn(adds, [1.0])
tf.map_fn(adds, [1.0]).eval()
tf.map_fn(adds, [1.0, 2.0, 3.0]).eval()
tf.argmax(tf.map_fn(adds, [1.0, 2.0, 3.0])).eval()
tf.argmax(tf.map_fn(adds, [1.0, 2.0, 3.0]).eval())
tf.argmax(tf.map_fn(adds, [1.0, 2.0, 3.0]),0).eval()
tf.argmax(tf.map_fn(adds, [1.0, 2.0, 3.0]), 0).eval()
tf.reduce_max(tf.map_fn(adds, [1.0, 2.0, 3.0]), 0).eval()


import tensorflow as tf
sess = tf.InteractiveSession()
actions = [{'id': 0, 'action': -1},
           {'id': 1, 'action': 0},
           {'id': 2, 'action': 1}]

s = tf.placeholder(tf.float32, [None, 10])
a = tf.placeholder(tf.float32, [None, 1])

def initialize_q(s, a):
    theta = tf.Variable(tf.zeros([11,1]))
    b = tf.Variable(tf.zeros([1]))
    m = tf.concat(1, [s, a])
    q = -tf.nn.relu(tf.matmul(m, theta) + b)
    return q
q = initialize_q(s, a)
sess.run(tf.initialize_all_variables())
sess.run(q, feed_dict={s: [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]], a: [[0.0]]})

# ---------------------------------------------------------------------------

import tensorflow as tf
sess = tf.InteractiveSession()
S = tf.placeholder(tf.float32, [None, 10])
  A = tf.placeholder(tf.float32, [None, 1])
Q = initialize_Q(S, A)
def initialize_Q(S, A):
    """ Initialize action-value function Q
    Args:
    S: state placeholder
    A: action placeholder

    Returns:
    Q: action-value function Q initialized with random weights \theta
    """

    theta = tf.Variable(tf.zeros([(S.get_shape()[1] + 1), 1]))
    b = tf.Variable(tf.zeros([1]))

    # m = tf.concat(1, [s, a])
    # q = -tf.nn.relu(tf.matmul(m, theta) + b)

    def Q(S, A):
        m = tf.concat(1, [S, A])
        return -tf.nn.relu(tf.matmul(m, theta) + b)

    return Q
Q = initialize_Q(S, A)
Q
Q()
Q(S,A)
def Qs(A):
    return Q(S,A)
qs = Qs(A)
qs
s = [[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]]
a = [[0.0]]
sess.run(qs, feed_dict={S: s, A: a})
sess.run(tf.initialize_all_variables())
sess.run(qs, feed_dict={S: s, A: a})
#history



[[float(a['id'])] for a in actions]

# ----------------------------------------------------------------

import tensorflow as tf
sess.run(tf.identity([1.0]))
sess = tf.InteractiveSession()
sess.run(tf.identity([1.0]))
def I(x):
    return tf.identity(x)
sess.run(tf.map_fn(I, [0.0, 1.0]))
sess.run(tf.map_fn(I, [[0.0], [1.0]]))
def C(x):
    return tf.concat(0, [[1.0], x])
sess.run(tf.map_fn(C, [[0.0], [1.0]]))
history


import tensorflow as tf
sess = tf.InteractiveSession()
S = tf.placeholder(tf.float32, [None, 1])
A = tf.placeholder(tf.float32, [None, 1])
sess.run(tf.concat(1, [S,A]), feed_dict={S:[[1.0]], A:[[0.0]]})
def f(x):
    return tf.concat(1, [S,x])
sess.run(tf.map_fn(f, [[[0.0]], [[1.0]]]), feed_dict={S:[[10.0]]})
history


In [11]: [[[float(a['id'])]] for a in actions]
Out[11]: [[[0.0]], [[1.0]], [[2.0]]]
