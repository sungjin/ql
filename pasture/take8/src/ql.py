# -*- coding: utf-8 -*-

# state = trail + current position.
# How about adding a hidden layer?

import tensorflow as tf
import random
import math
import logging
import sys

import sine


# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

MAX_NUM_OF_EP = 1000000
MAX_TIME_STEP = 200
NUM_OF_TRAIL = 10
NUM_OF_ACTION = 3

actions = [-1, 0, 1]


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def initial_s():
    tr = sine.trace(1)
    p = sine.f(1)

    tr.append(p)

    return tr


def execute_action(s, a, t, actions):
    """
    Args:
    s: state
    a: action id
    t: time
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:
    r: immediate reward
    s_prime: next state

    """

    p = s[-1]
    delta = float(actions[a])
    p_prime = p + delta

    # # limit the range for p. without this it takes too long to
    # # converge.
    # if (p_prime < 0.0):
    #     p_prime = 0.0
    # elif (p_prime > 120.0):
    #     p_prime = 120.0

    # Does not converge! Let's limit the range of p more severely.
    if (p_prime >= sine.f(t + 1) + 20.0):
        p_prime = sine.f(t + 1) + 20.0
    elif (p_prime <= sine.f(t + 1) - 20.0):
        p_prime = sine.f(t + 1) - 20.0

    r = - abs(math.pow((sine.f(t + 1) - p_prime), 2))

    tr = sine.trace(t + 1)
    tr.append(p_prime)

    return r, tr


# Aux function is ugly by nature!
def replay(ep, argmax_Q, S, actions, sess):
    f = open("./result/r%010d" % ep, 'w')

    s = initial_s()

    # actions = [-1, 0, 1]

    for t in range(1, MAX_TIME_STEP):
        p = s[-1]

        f.write("%d\t%f\n" % (t, p))

        a_a = sess.run(argmax_Q, feed_dict={S: [s]})
        a = a_a[0]
        # print('replay: selected action id: ' + str(a))

        _, s = execute_action(s, a, t, actions)


def Q_learning(actions):
    """ Q-learning algorithm function

    Args:
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:

    """
    # learning rate
    alpha = 0.001
    # discount rate
    gamma = 0.9
    # epsilon for \varepsilon greedy selection
    epsilon = 0.1

    # S 1x(10 + 1)
    S = tf.placeholder(tf.float32, [1, NUM_OF_TRAIL + 1])
    A = tf.placeholder(tf.int32, [])
    Y = tf.placeholder(tf.float32, [])

    # theta 10x3
    theta_h = weight_variable([NUM_OF_TRAIL + 1, NUM_OF_TRAIL + 1])
    theta = weight_variable([NUM_OF_TRAIL + 1, NUM_OF_ACTION])
    # b 1x3
    b_h = bias_variable([1, NUM_OF_TRAIL + 1])
    b = bias_variable([1, NUM_OF_ACTION])

    H = tf.nn.relu(tf.matmul(S, theta_h) + b_h)
    Q = -tf.nn.relu(tf.matmul(H, theta) + b)

    argmax_Q = tf.argmax(Q, 1)
    max_Q = tf.reduce_max(Q, 1)

    L = tf.pow(tf.sub(Y, tf.slice(Q, [0, A], [1, 1])), 2)

    # optimizer = tf.train.GradientDescentOptimizer(alpha).minimize(L)
    optimizer = tf.train.RMSPropOptimizer(alpha).minimize(L)

    # sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=False))
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        # logging.debug('episode #= ' + str(ep))
        print('episode #= ' + str(ep))

        #   \STATE Initilize state $s_1$
        s = initial_s()

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):
            logging.debug('t = ' + str(t)
                          + '  ------------------------------------------'
                          + '-----------')

            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}

            logging.debug('Q = \t\t' + str(sess.run(Q, feed_dict={S: [s]})))

            e = random.uniform(0, 1)
            if (e < epsilon):
                a = random.randrange(0, NUM_OF_ACTION)
            else:
                a_a = sess.run(argmax_Q, feed_dict={S: [s]})
                a = a_a[0]

            # a = select_action(Q, S, s, sess, argmax_Q)
            logging.debug('selected action id: ' + str(a))
            # print('selected action id: ' + str(a))

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state
            r, s_prime = execute_action(s, a, t, actions)

            logging.debug('reward = ' + str(r))
            # logging.debug('s_prime = ' + str(s_prime))
            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$

            if (t + 1 >= MAX_TIME_STEP):
                y = r
                logging.debug('At the end of ep:')
            else:
                logging.debug('Q_prime = \t'
                              + str(sess.run(Q,
                                             feed_dict={S: [s_prime]})))
                q_a = sess.run(max_Q, feed_dict={S: [s_prime]})
                max_q = q_a[0]

                y = r + gamma * max_q
            logging.debug('y = ' + str(y))

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$

            sess.run(optimizer, feed_dict={S: [s], A: a, Y: y})

            s = s_prime
        #   \ENDFOR

        # replay at each end of the episode.
        replay(ep, argmax_Q, S, actions, sess)
    #   \ENDFOR


def main():
    Q_learning(actions)


if __name__ == "__main__":
    main()
