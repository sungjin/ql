# -*- coding: utf-8 -*-

# state = trail + current position.
# How about adding a hidden layer?

# Wow we've gone so far.
# Added two hidden layer.
# And experience replay (very basic),
# and target action-value function.
# ENABLED: clip function implemented (although very primitive).
# DISABLED: 'Game over'.
# HIGH PENALTY i.e., high negative reward.
# DISABLED: decaying learning rate.
# ENABLED: decaying epsilon
# If this fails,
#
# I'll be very sad.

import tensorflow as tf
import random
import logging
# import sys
from collections import deque
import sine


# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

MAX_NUM_OF_EP = 1000000
MAX_TIME_STEP = 800
NUM_OF_TRAIL = 10
NUM_OF_ACTION = 3

CUT_SCORE = -1000.0

MAX_DEQUE_LEN = 200

actions = [-1, 0, 1]

score = 0.0


def weight_variable(shape):
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def initial_s():
    global score
    score = 0.0

    tr = sine.trace(1)
    p = sine.f(1)

    tr.append(p)

    s = tr

    return s


# store_transition(D, s, a, r, s_prime, game_over)
def store_transition(D, transition):
    D.append(transition)

    if (len(D) > MAX_DEQUE_LEN):
        D.popleft()

    # I know returning D is redundant, but this is more clear (I think).
    return D


# s, a, r, s_prime, game_over = sample_batch(D)
def sample_batch(D):

    i = random.randrange(len(D))
    return D[i]


def execute_action(s, a, t, actions):
    """
    Args:
    s: state
    a: action id
    t: time
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:
    r: immediate reward
    s_prime: next state

    """

    global score

    game_over = False

    p = s[-1]
    delta = float(actions[a])
    p_prime = p + delta

    # limit the range for p. without this it takes too long to
    # converge.
    if (p_prime < 0.0):
        p_prime = 0.0
    elif (p_prime > 120.0):
        p_prime = 120.0

    # min_f = sine.f(t + 1) - 10.0
    # max_f = sine.f(t + 1) + 10.0

    # if (p_prime < min_f):
    #     p_prime = min_f
    # elif (p_prime > max_f):
    #     p_prime = max_f

    # r = max(- abs(sine.f(t + 1) - p_prime), -10.0)
    # r = - (sine.f(t + 1) - p_prime) * (sine.f(t + 1) - p_prime)
    # r = - abs(sine.f(t + 1) - p_prime)
    r = - abs(sine.f(t + 1) - p_prime)

    tr = sine.trace(t + 1)
    tr.append(p_prime)

    # If we stay the same state endlessly?
    # tr = sine.trace(t)
    # tr.append(p)

    s_prime = tr

    # Early stop when the score is too low.
    score += r

    # if ((t + 1 >= MAX_TIME_STEP) or (score < CUT_SCORE)):
    if ((t + 1 >= MAX_TIME_STEP)):
        game_over = True

    return r, s_prime, game_over


# Aux function is ugly by nature!
def replay(ep, argmax_Q, S, actions, sess):
    f = open("./result/r%010d" % ep, 'w')
    f2 = open("./result-cur", 'w')

    s = initial_s()
    game_over = False

    # actions = [-1, 0, 1]

    for t in range(1, MAX_TIME_STEP):
        if game_over:
            break

        p = s[-1]

        f.write("%d\t%f\n" % (t, p))
        f2.write("%d\t%f\n" % (t, p))

        a, = sess.run(argmax_Q, feed_dict={S: [s]})

        _, s, game_over = execute_action(s, a, t, actions)


def Q_learning(actions):
    """ Q-learning algorithm function

    Args:
    actions: a tuple (action_1, action_2, ..., action_n)

    Returns:

    """

    # learning rate
    alpha = 0.001
    # discount rate
    gamma = 0.9

    # epsilon for \varepsilon greedy selection
    epsilon = 0.1

    # number of steps to reset \hat{Q}
    C = 50

    #   \STATE Initilize replay memory
    D = deque()

    # S 1x(10 + 1)
    S = tf.placeholder(tf.float32, [1, NUM_OF_TRAIL + 1])
    A = tf.placeholder(tf.int32, [])
    Y = tf.placeholder(tf.float32, [])

    # theta 10x3
    # theta_h = weight_variable([NUM_OF_TRAIL + 1, NUM_OF_TRAIL + 1])
    # theta_h1 = weight_variable([NUM_OF_TRAIL + 1, NUM_OF_TRAIL + 1])
    # theta_h2 = weight_variable([NUM_OF_TRAIL + 1, NUM_OF_TRAIL + 1])
    theta_h1 = weight_variable([NUM_OF_TRAIL + 1,
                                4 * (NUM_OF_TRAIL + 1)])
    theta_h2 = weight_variable([4 * (NUM_OF_TRAIL + 1),
                                4 * (NUM_OF_TRAIL + 1)])
    theta = weight_variable([4 * (NUM_OF_TRAIL + 1),
                             NUM_OF_ACTION])

    # theta_h_t = tf.Variable(theta_h.initialized_value())
    theta_h1_t = tf.Variable(theta_h1.initialized_value(), trainable=False)
    theta_h2_t = tf.Variable(theta_h2.initialized_value(), trainable=False)
    theta_t = tf.Variable(theta.initialized_value(), trainable=False)

    # b 1x3
    # b_h = bias_variable([1, NUM_OF_TRAIL + 1])
    # b_h1 = bias_variable([1, NUM_OF_TRAIL + 1])
    # b_h2 = bias_variable([1, NUM_OF_TRAIL + 1])
    b_h1 = bias_variable([1, 4 * (NUM_OF_TRAIL + 1)])
    b_h2 = bias_variable([1, 4 * (NUM_OF_TRAIL + 1)])
    b = bias_variable([1, NUM_OF_ACTION])

    # b_h_t = tf.Variable(b_h.initialized_value())
    b_h1_t = tf.Variable(b_h1.initialized_value(), trainable=False)
    b_h2_t = tf.Variable(b_h2.initialized_value(), trainable=False)
    b_t = tf.Variable(b.initialized_value(), trainable=False)

    # H = tf.nn.relu(tf.matmul(S, theta_h) + b_h)
    # Q = -tf.nn.relu(tf.matmul(H, theta) + b)
    H1 = tf.nn.relu(tf.matmul(S, theta_h1) + b_h1)
    H2 = tf.nn.relu(tf.matmul(H1, theta_h2) + b_h2)
    Q = -tf.nn.relu(tf.matmul(H2, theta) + b)

    # H_t = tf.nn.relu(tf.matmul(S, theta_h_t) + b_h_t)
    # Q_t = -tf.nn.relu(tf.matmul(H_t, theta_t) + b_t)
    H1_t = tf.nn.relu(tf.matmul(S, theta_h1_t) + b_h1_t)
    H2_t = tf.nn.relu(tf.matmul(H1_t, theta_h2_t) + b_h2_t)
    Q_t = -tf.nn.relu(tf.matmul(H2_t, theta_t) + b_t)

    argmax_Q = tf.argmax(Q, 1)
    # max_Q = tf.reduce_max(Q, 1)
    max_Q_t = tf.reduce_max(Q_t, 1)

    L = tf.pow(tf.sub(Y, tf.slice(Q, [0, A], [1, 1])), 2)

    # ??? is this so difficult to implement?
    # Let's modify this loss term as suggested by the paper.
    # diff = tf.sub(Y, tf.slice(Q, [0, A], [1, 1]))
    # clip = tf.minimum(1.0, tf.maximum(-1.0, diff))
    # L = tf.pow(clip, 2)

    # reset_Q_t = [theta_h_t.assign(theta_h), theta_t.assign(theta),
    #              b_h_t.assign(b_h), b_t.assign(b)]
    reset_Q_t = [theta_h1_t.assign(theta_h1),
                 theta_h2_t.assign(theta_h2),
                 theta_t.assign(theta),
                 b_h1_t.assign(b_h1),
                 b_h2_t.assign(b_h2),
                 b_t.assign(b)]

    # global_step = tf.Variable(0, trainable=False)
    # starter_learning_rate = 0.1
    # learning_rate = tf.maximum(
    #     alpha,
    #     tf.train.exponential_decay(starter_learning_rate,
    #                                global_step,
    #                                100000,
    #                                0.96,
    #                                staircase=False))

    # optimizer = tf.train.RMSPropOptimizer(learning_rate=alpha, epsilon=1e-5)
    optimizer = tf.train.RMSPropOptimizer(learning_rate=alpha)
    minimizer = optimizer.minimize(L)
    # optimizer = tf.train.RMSPropOptimizer(learning_rate)
    # minimizer = optimizer.minimize(L, global_step=global_step)

    # sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
    sess = tf.Session(config=tf.ConfigProto(log_device_placement=False))
    sess.run(tf.initialize_all_variables())

    #   \FOR{$\text{episode} = 1, M$}
    for ep in range(1, MAX_NUM_OF_EP):
        # logging.debug('episode #= ' + str(ep))
        if (ep % 100) == 1:
            print('episode #= ' + str(ep))

            epsilon = max(pow(0.9, ep / 100.0), 0.1)
            
        ep_terminated = False

        total_loss = 0.0

        #   \STATE Initilize state $s_1$
        s = initial_s()

        #   \FOR{$t=1, T$}
        for t in range(1, MAX_TIME_STEP):
            logging.debug('t = ' + str(t)
                          + '  ------------------------------------------'
                          + '-----------')

            #   \STATE With probability $\varepsilon$ select a random
            #   action $a_t$
            #   \STATE otherwise select $a_t = \argmax_a Q(\phi (s_t  ), a ;
            #   \theta)$ \COMMENT{$\varepsilon$-greedy}

            logging.debug('Q = \t\t' + str(sess.run(Q, feed_dict={S: [s]})))

            e = random.uniform(0, 1)
            if (e < epsilon):
                a = random.randrange(0, NUM_OF_ACTION)
            else:
                a, = sess.run(argmax_Q, feed_dict={S: [s]})

            logging.debug('selected action id: ' + str(a))
            # print('selected action id: ' + str(a))

            #   \STATE Execute action $a_t$ and observe reward $r_t$ and state
            r, s_prime, game_over = execute_action(s, a, t, actions)

            # We've got a spaghetti!
            if game_over:
                ep_terminated = True

            # We've got more spaghetti!
            s_prime_save = s_prime

            D = store_transition(D, (s, a, r, s_prime, game_over))

            (s, a, r, s_prime, game_over) = sample_batch(D)

            logging.debug('reward = ' + str(r))

            #   \STATE Set $y_j =
            #   \begin{cases} r_j & \text{if episode terminates at step } j+ 1
            #     \\ r_j + \gamma \max_{a'} Q\left(\phi_{j+1} , a' ;
            #   \theta \right)& \text{otherwise} \end{cases}$

            if game_over:
                y = r
                logging.debug('Game over!')
                # The game went through? or we retired the race?
                # if (t + 1 < MAX_TIME_STEP):
                #     print("We couldn't make it!")
            else:
                logging.debug('Q_prime = \t'
                              + str(sess.run(Q,
                                             feed_dict={S: [s_prime]})))

                max_q, = sess.run(max_Q_t, feed_dict={S: [s_prime]})

                y = r + gamma * max_q
            logging.debug('y = ' + str(y))

            #   \STATE Perform a gradient descent step on $\left( y_j -
            #   Q\left(\phi_j ,
            #     a_j ; \theta \right) \right)^2$ with respect to the
            #   network
            # parameters $\theta$
            sess.run(minimizer, feed_dict={S: [s], A: a, Y: y})

            s = s_prime_save

            if (ep % 100) == 1:
                total_loss += sess.run(L, feed_dict={S: [s], A: a, Y: y})

            #   \STATE Every C steps reset $\hat{Q} = Q$
            if (t % C) == 0:
                sess.run(reset_Q_t)

            if ep_terminated:
                break
            # if game_over:
            #     break
        #   \ENDFOR

        if (ep % 100) == 1:
            print('average loss: %f' % (total_loss / t))

        # replay at each end of the episode.
        replay(ep, argmax_Q, S, actions, sess)
    #   \ENDFOR


def main():
    Q_learning(actions)


if __name__ == "__main__":
    main()
