\documentclass[serif,aspectratio=169]{beamer}
\usepackage{mathpazo}


% \setcounter{section}{-1}
\mode<presentation>

\usepackage{algorithm}
\usepackage{algorithmic}

\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}


\title{Q-Learning:}
\subtitle{or, Playing Atari with Deep Reinforcement Learning}

\author{Sungjin Kim, \\with thanks to the original authors}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}

\section{Prelude}
\frame{\sectionpage}

\begin{frame}
  \frametitle{It all happened with just a little curiosity.}
  \begin{figure}
    \centering
    \includegraphics[height=2.0in]{readinglist.png}
    % \caption{Reading lists}
  \end{figure}


\end{frame}
\begin{frame}
  \frametitle{And that paper.}
  \begin{figure}
    \centering
    \includegraphics[height=2.0in]{paper.png}
    % \caption{Reading lists}
  \end{figure}

  \url{https://www.youtube.com/watch?v=iqXKQf2BOSE}
\end{frame}

\begin{frame}
  \frametitle{Concepts}
  Reinforcement learning, Bellman equation, Markov decision process,
  value-iteration vs. policy iteration, Q-learning, off-policy
  vs. on-policy, exploration vs. exploitation, experience replay,
  RMSProp algorithm, $\varepsilon$-greedy \ldots and much more.
\end{frame}


% \begin{frame}[shrink=30]
\begin{frame}
  \frametitle{Math}
  The optimal action-value function (defined somewhat recursively):
  \begin{equation}
    Q^*(s,a) = \mathbb{E}_{s' \sim \mathcal{E}} \left[ r + \gamma
      \max_{a'} Q^* (s', a') \middle\vert s, a \right ]
  \end{equation}

  Loss functions
  \begin{equation}
    L_i (\theta_i) = \mathbb{E}_{s,a \sim \rho (\cdot)} \left[ (y_i -
      Q (s, a; \theta_i))^2 \right ]
  \end{equation}

  Differentiating the loss function we arrive at the gradient

  \begin{multline}
    \nabla_{\theta_i} L_i (\theta_i) = \\
    \mathbb{E}_{s,a \sim \rho(
      \cdot ) ; s' \sim \mathcal{E} } \left[ \left( r + \gamma
        \max_{a'} Q(s', a' ; \theta_{i - 1} ) - Q(s,a;\theta_i)
      \right) \nabla_{\theta_i} Q(s,a; \theta_i) \right ]
  \end{multline}
  % \begin{equation}
  %   \nabla_{\theta_i} L_i (\theta_i) = \mathbb{E}_{s,a \sim \rho(
  %   \cdot ) ; s' \sim \mathcal{E} } \left[ \left( r + \gamma
  %       \max_{a'} Q(s', a' ; \theta_{i - 1} ) - Q(s,a;\theta_i)
  %     \right) \nabla_{\theta_i} Q(s,a; \theta_i) \right ]
  % \end{equation}

  
\end{frame}

\section{Introduction}
\frame{\sectionpage}

\begin{frame}
  \frametitle{Introducing reinforcement learning}

  \begin{itemize}
  \item It is a kind of machine learning.
  \item However not a kind of supervised learning, nor
    of unsupervised learning.
  \end{itemize}
  
  Excerpt from Prof. Sutton's textbook:
  
  \begin{quote}
    Reinforcement learning problems involve learning what to do---how to
    map \alert{situations}\footnote{In fact, \emph{state} is more commonly
      used. Also used in this paper is the term, \emph{sequence}.} to \alert{actions}---so as to maximize a
    numerical \alert{reward signal}.
  \end{quote}

\end{frame}

\begin{frame}
  \frametitle{Challenges of reinforcement learning}
  \begin{itemize}
  \item  Training data.
    \begin{itemize}
    \item Most deep learning applications requires large amounts of
      hand-labeled training data.
    \item RL algorithms must be able to learn from scalar reward
      signal that is frequently sparse, noisy and \alert{delayed}.
    \end{itemize}
  \item In RL one typically encounters sequences
    of highly \alert{correlated} states.
  \item In RL the data distribution \alert{changes} as the algorithm learns
    \alert{new} behaviors (we can not assume a fixed underlying distribution).
  \end{itemize}
\end{frame}


\section{Background}
\frame{\sectionpage}

\begin{frame}
  \frametitle{General descriptions}
  \begin{itemize}
  \item Consider tasks in which an agent interacts with an environment
    $\mathcal{E}$, in a sequence of \alert{actions}, \alert{observations}
    and \alert{rewards}.
  \item At each time-step the agent select an \alert{action} $a_t$
    from the set of legal (game) actions, $\mathcal{A} = \{1, \ldots,
    K\}$.
  \item The agent \alert{observes} an image $x_t \in \mathbb{R}^d$.
  \item The agent receives a \alert{reward} $r_t$ representing the change in
    game score.
    \begin{itemize}
    \item Note that the feedback about an action may only be received
      after many thousands of time-steps have elapsed.
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Markov decision process}
  \begin{itemize}
  \item The agent only observes images of the current screen.
    \begin{itemize}
    \item The task is partially observed\footnote{In fact, there is a
        formal definition of a POMDP.}
    \item it is impossible to fully understand from only the current
      screen $x_t$.
    \end{itemize}
  \item therefore consider sequences of actions and observations,
    $s_t = x_1, a_1, x_2, \ldots, a_{t-1}, x_t$.
  \item All sequences are assumed to terminate in a finite number
    of time-steps.
  \end{itemize}

  This formalism gives rise to a large but finite \emph{Markov decision process}.
\end{frame}

\begin{frame}
  \frametitle{The name of the game}

  \begin{itemize}
  \item The goal of the agent is to maximize future \alert{rewards}.
  \item Future rewards are discounted by a factor of $\gamma$ per
    time-step.
  \end{itemize}

  Future discounted \alert{return} at time $t$:
  \begin{equation}
    R_t = \sum_{t' = t}^{T} \gamma^{t' -t} r_{t'}
  \end{equation}

  Optimal action-value function $Q^* (s,a)$ as the maximum expected
  return achievable by following any strategy, after seeing some
  sequence $s$ and then taking some action $a$:
  \begin{equation}
    Q^* (s,a) = \max_\pi \mathbb{E} \left[ R_t | s_t = s, a_t = a, \pi \right]
  \end{equation}
  where $\pi$ is a policy mapping sequences to actions.
\end{frame}

\begin{frame}
  \frametitle{Bellman equation}
  If the optimal value $Q^*(s', a')$ of the sequence $s'$ at the next
  time step was known for all possible actions $a'$, then the optimal
  strategy is to select the action $a'$ maximizing the expected value
  of $r + \gamma Q^* (s', a')$.

  \begin{equation}
    Q^*(s,a) = \mathbb{E}_{s' \sim \mathcal{E}} \left[ r + \gamma
      \max_{a'} Q^* (s', a') \middle\vert s, a \right ]
  \end{equation}

\end{frame}

\begin{frame}
  \frametitle{Value iteration}
  The recursive nature of the Bellman equation:
  \begin{equation}
    \alert{Q^*(s,a)} = \mathbb{E}_{s' \sim \mathcal{E}} \left[ r + \gamma
      \max_{a'} \alert{Q^* (s', a')} \middle\vert s, a \right ]
  \end{equation}

  Use the equation as an iterative update:
  \begin{equation}
    Q_{\alert{i+1}} (s,a) = \mathbb{E} \left[ r + \gamma \max_{a'} Q_{\alert{i}} (s',
      a') \middle| s,a  \right]
  \end{equation}

  Such \emph{value iteration} algorithms converge to optimal action-value
  function, $Q_i \to Q^*$ as $i \to \infty$. However, in practice, this basic
  approach is impractical, because the action-value function is
  estimated separately for each sequence, without any
  \alert{generalisation}. 

  % \url{https://www.youtube.com/watch?v=Xoi8T1ElxnU}
  
  
\end{frame}

\begin{frame}
  \frametitle{Value iteration in action}
  \begin{figure}
    \centering
    \includegraphics[height=2.0in]{exact.png}
    \caption{\url{https://www.youtube.com/watch?v=Xoi8T1ElxnU}}
  \end{figure}

% https://www.youtube.com/watch?v=by8y7xRO1Js

\end{frame}


\begin{frame}
  \frametitle{Q-network}
  Use a function approximator to estimate the action-value function,
  \begin{equation}
    Q(s,a;\theta) \approx Q^* (s, a)
  \end{equation}
  Then the Q-network can be trained by minimising a sequence of loss
  function $L_i(\theta_i)$, 
  \begin{equation}
    L_i (\theta_i) = \mathbb{E}_{s,a \sim \rho (\cdot)} \left[ (y_i -
      Q (s, a; \theta_i))^2 \right ]
  \end{equation}
  where 
  \begin{equation}
    y_i = \mathbb{E}_{s' \sim \mathcal{E}} \left[ r + \gamma
      \max_{a'} Q (s', a'; \theta_{\alert{i-1}}) \middle\vert s, a \right ]
  \end{equation}
  and $\rho(s,a)$ is a probability distribution refered as the behavior
  distribution.

  Differentiating the loss function we arrive at the following
  gradient,
  \begin{multline}
    \nabla_{\theta_i} L_i (\theta_i) = \\
    \mathbb{E}_{s,a \sim \rho(
      \cdot ) ; s' \sim \mathcal{E} } \left[ \left( r + \gamma
        \max_{a'} Q(s', a' ; \theta_{i - 1} ) - Q(s,a;\theta_i)
      \right) \nabla_{\theta_i} Q(s,a; \theta_i) \right ]
  \end{multline}
  % \begin{equation}
  %   \nabla_{\theta_i} L_i (\theta_i) = \mathbb{E}_{s,a \sim \rho(
  %   \cdot ) ; s' \sim \mathcal{E} } \left[ \left( r + \gamma
  %       \max_{a'} Q(s', a' ; \theta_{i - 1} ) - Q(s,a;\theta_i)
  %     \right) \nabla_{\theta_i} Q(s,a; \theta_i) \right ]
  % \end{equation}

  
\end{frame}

\begin{frame}
  \frametitle{Q-network in action}
  \begin{figure}
    \centering
    \includegraphics[height=2.0in]{approx.png}
    \caption{\url{https://www.youtube.com/watch?v=by8y7xRO1Js}}
  \end{figure}

\end{frame}


\begin{frame}
  \frametitle{Commentary note}

  \begin{itemize}
  \item The algorithm is \emph{model-free}: it solves the
    reinforcement learning task directly, without constructing an
    estimate\footnote{A state transition function is
      not given.} of $\mathcal{E}$.
  \item It is also \emph{off-policy}: it learns about the greedy
    strategy $a = \argmax_a Q(s,a; \theta)$, while following a behavior
    distribution that ensures adequate \alert{exploration} of the
    state space.
  \item In practice the behavior distribution is often selected by
    an $\varepsilon$-greedy strategy;
    \begin{itemize}
    \item follows the greedy strategy with probability $1-
      \varepsilon$
    \item selects a random action with probability $\varepsilon$
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Deep Reinforcement Learning}
\frame{\sectionpage}

\begin{frame}
  \frametitle{Experience replay}
  \begin{itemize}
  \item   Store the agent's experiences at each time-step, $e_t = (s_t, a_t,
    r_t, s_{t+1} )$ in a data-set $ \mathcal{D} = \{ e_1, e_2, \ldots,
    e_N \} $, pooled over many episodes into a \emph{replay memory}.
  \item During the inner loop of the algorithm,  apply Q-learning updates,
    or minibatch updates, to samples of experience, $e \sim \mathcal{D}$.
  \end{itemize}

  Advantages:
  \begin{itemize}
  \item Each step of experience is potentially used in many weight
    updates, which allows for greater data efficiency.
  \item Learning directly from consecutive samples is inefficient,
    due to the strong correlations between the samples.
  \item When learning on-policy the current parameters determine the
    next data sample that the parameters are trained on.
  \end{itemize}
  
\end{frame}

\begin{frame}
  \frametitle{Separating target values\footnote{In fact, this modification was
      presented in the sequel, ``Human-level control through deep
      reinforcement learning''.}}
  Every $C$ updates, clone the network $Q$ to obtain target network
  $\hat{Q}$ and use $\hat{Q}$ for generating the Q-learning targets
  $y_j$ for the following $C$ updates to $Q$.
  \begin{itemize}
  \item This makes the algorithm more stable compared to standard
    online Q-learning, where an update that increases $Q(s_t, a_t)$
    often also increases $Q(s_{t+1}, a)$ for all $a$ hence also
    increases the target $y_j$, possibly leading to oscillations or
    divergence of the policy.
    \item Generating the targets using an older set of parameters adds
      a delay between the time an update to $Q$ is made and the time
      the update affects the targets $y_j$.
  \end{itemize}
\end{frame}
\begin{frame}
  \frametitle{Preprocessing}
  In DQN, a preprocessing function $\phi$ is used. Its purpose is
  mainly twofold.
  
  \begin{itemize}
  \item to reduce input dimensionality
    \begin{itemize}
    \item 210 $\times$ 160 pixel RGB to 84 $\times$ 84 gray-scale.
    \end{itemize}
    \item (More importantly) to \alert{stack} the last 4 frames of a
      history.
      
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Figure}
  \begin{figure}
    \centering
    \includegraphics[height=2.5in]{dqn.png}
    % \caption{Reading lists}
  \end{figure}
\end{frame}

\begin{frame}[shrink=20]
  \frametitle{Algorithm}
  \begin{algorithm}[H]
    \caption{deep Q-learning with experience replay (preprocessing is omitted)}
    \begin{algorithmic}
      \STATE {\color{blue}{Initialize replay memory $D$ to capacity $N$}}
      \STATE Initialize action-value function $Q$ with random weights $\theta$
      \STATE {\color{blue}Initialize target action-value function $\hat{Q}$ with
      weights $\theta^- = \theta$}
      \FOR{$\text{episode} = 1, M$}
      \STATE Initialize sequence $s_1 = \{x_1\}$ and {\color{blue} preprocessed
      sequence $\phi_1 = \phi(s_1)$}
      \FOR{$t=1, T$}
      \STATE With probability $\varepsilon$ select a random action $a_t$
      \STATE otherwise select $a_t = \argmax_a Q(\phi(s_t) , a ;
      \theta)$ \alert{\COMMENT{$\varepsilon$-greedy}}
      \STATE Execute action $a_t$ and observe reward $r_t$ and image
      $x_{t+1}$
      \STATE Set $s_{t+1} = s_t, a_t, x_{t+1}$ and {\color{blue}
        preprocess $\phi_{t+1} = \phi(s_{t+1}) $}
      \STATE {\color{blue}Store transition $(\phi_t, a_t, r_t, \phi_{t+1})$
        in $D$}
      \STATE {\color{blue} Sample random minibatch of transitions
        $\left( \phi_{\alert{j}}, a_{\alert{j}}, r_{\alert{j}}, \phi_{\alert{j+1}} \right)$ from $D$}
      \STATE Set $y_j =
      \begin{cases} r_j & \text{if episode terminates at step } j+ 1
        \\ r_j + \gamma \max_{a'} \hat{Q}\left(\phi_{j+1} , a' ; \theta^- \right)& \text{otherwise} \end{cases}$
      \STATE Perform a gradient descent step on $\left( y_j - Q\left(\phi_j ,
          a_j ; \theta \right) \right)^2$ with respect to the network
      parameters $\theta$
      \STATE {\color{blue}Every $C$ steps reset $\hat{Q} = Q$}
      \ENDFOR
      \ENDFOR
    \end{algorithmic}
  \end{algorithm}
  
\end{frame}

\begin{frame}
  \frametitle{Miscellany}
  \begin{itemize}
  \item   RMSProp: a gradient descent optimization algorithm suggested
    by Prof. Hinton.
  \item $\varepsilon$ annealing: $\varepsilon$ annealed linearly from 1
    to 0.1 over certain frames, and fixed at 0.1 thereafter.
    \item clipping the error term: clip the error term from the update
      $r + \gamma \max_{a'} Q(s', a' ; \theta_i^-) - Q(s,a; \theta_i)$
      to be between -1 and 1.
      \begin{itemize}
      \item the absolute value loss function $|x|$ has a derivative -1
        for all negative values of $x$ and a derivative 1 for all
        positive values of $x$
      \item clipping the squared error to be between -1 and 1
        corresponds to using an absolute value loss function for
        errors outside of the (-1,1) interval.
      \item i.e., switch the loss function to $|x|$ for errors outside
        of the (-1,1) interval.
      \end{itemize}
  \end{itemize}

\end{frame}
\end{document}

